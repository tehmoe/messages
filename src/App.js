import React, { Component } from "react";
import styled from "styled-components";

import Messages from "./components/Messages/Messages";
import Requests from "./components/Requests/Requests";

const AppWrapper = styled.div`
  display: flex;
`;
class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Messages />
        <Requests />
      </AppWrapper>
    );
  }
}

export default App;
