import * as io from "socket.io-client";
import { LOAD_MESSAGES_RECEIVE_DATA } from "../constants/constants";

export const loadMessages = () => {
  return dispatch => {
    const socket = io("https://chat-code-test.herokuapp.com/", {
      transports: ["websocket"],
      "force new connection": true,
      forceNew: true,
      reconnectionAttempts: Infinity,
      reconnect: true,
      reconnection: true,
    });

    socket.on("connect", () => {
      socket.on("MESSAGE_RECEIVED", data => {
        dispatch({
          type: LOAD_MESSAGES_RECEIVE_DATA,
          message: data,
        });
      });
    });
  };
};
