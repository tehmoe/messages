import React from "react";
import styled from "styled-components";

import { COLOURS } from "../../constants/constants";

const StyledMessage = styled.div`
  background-color: ${COLOURS.blue};
  color: #ffffff;
  padding: 15px;
  border-radius: 5px;
  width: 600px;
  margin-bottom: 20px;
`;
export default ({ message }) => {
  return <StyledMessage>{message.message}</StyledMessage>;
};
