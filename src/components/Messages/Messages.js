import React, { Component } from "react";
import styled from "styled-components";

import Message from "../Message/Message";
import MessagesContainer from "../../containers/MessagesContainer";

const MessagesWrapper = styled.div`
  flex: 2;
  max-height: 100vh;
  overflow-y: scroll;
  margin-top: 10px;
`;

class Messages extends Component {
  componentDidMount() {
    this.props.loadMessages();
  }
  render() {
    return (
      <MessagesWrapper>
        {this.props.messages &&
          this.props.messages.map(message => (
            <Message key={message.id} message={message} />
          ))}
      </MessagesWrapper>
    );
  }
}
export default MessagesContainer(Messages);
