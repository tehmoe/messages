import React from "react";
import styled from "styled-components";

const StyledRequest = styled.span`
  padding: 10px;
`;
export default ({ name, count }) => {
  return <StyledRequest>{`${name}: ${count}`}</StyledRequest>;
};
