import React, { Component } from "react";
import styled from "styled-components";

import Request from "../Request/Request";
import RequestsContainer from "../../containers/RequestsContainer";

const RequestsWrapper = styled.div`
  flex: 1;
`;

class Requests extends Component {
  render() {
    return (
      <RequestsWrapper>
        {this.props.requests &&
          Object.keys(this.props.requests).map(
            key =>
              this.props.requests[key] ? (
                <Request
                  count={this.props.requests[key]}
                  name={key}
                  key={key}
                />
              ) : (
                ""
              ),
          )}
      </RequestsWrapper>
    );
  }
}
export default RequestsContainer(Requests);
