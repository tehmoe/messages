import { connect } from "react-redux";
import { messagesSelector } from "../selectors/selectors";
import { loadMessages } from "../actions/actions";
import { createStructuredSelector } from "reselect";

const mapStateToProps = createStructuredSelector({
  messages: messagesSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    loadMessages: option => dispatch(loadMessages(option)),
  };
}

const MessagesContainer = (WrappedComponent: Function) =>
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(WrappedComponent);

export default MessagesContainer;
