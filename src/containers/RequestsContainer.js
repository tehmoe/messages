import { connect } from "react-redux";
import { requestSelector } from "../selectors/selectors";
import { createStructuredSelector } from "reselect";

const mapStateToProps = createStructuredSelector({
  requests: requestSelector(),
});

const RequestsContainer = (WrappedComponent: Function) =>
  connect(
    mapStateToProps,
    null,
  )(WrappedComponent);

export default RequestsContainer;
