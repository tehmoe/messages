import { LOAD_MESSAGES_RECEIVE_DATA } from "../constants/constants";

const initialState = { messages: [] };

function messagesReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_MESSAGES_RECEIVE_DATA:
      return {
        messages: [...state.messages, action.message],
      };
    default:
      return state;
  }
}

export default messagesReducer;
