import { combineReducers } from "redux";
import messagesReducer from "./reducers/reducers";

export default combineReducers({
  messagesReducer,
});
