import { createSelector } from "reselect";
import { map, groupBy } from "lodash";

const messagingDomain = state => state.messagesReducer;

const messagesSelector = () =>
  createSelector(messagingDomain, substate => substate.messages);

const requestSelector = () =>
  createSelector(messagingDomain, substate => {
    const types = substate.messages.map(message => message.type);
    const groupedTypes = groupBy(types);
    let groupedByKey = [];

    map(groupedTypes, (item, key) => {
      groupedByKey[key] = item.length;
    });
    return groupedByKey;
  });

export { messagesSelector, requestSelector };
